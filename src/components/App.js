import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../REDUX/actions/actions';

class App extends Component {
  constructor() {
    super()

    this.valueOfAccount = 0
  }

  handleDepositMoney = (e) => {
    if (!this.validateString(this.valueOfAccount)) return

    const { depositMoney } = this.props.Actions;
    const { money } = this.props.main;

    let nextValue = money + this.valueOfAccount;
    depositMoney(nextValue);
  }

  handleWithdrawMoney = (e) => {
    if (!this.validateString(this.valueOfAccount)) return

    const { withdrawMoney } = this.props.Actions;
    const { money } = this.props.main;

    let nextValue = money - this.valueOfAccount;

    if(nextValue)
      withdrawMoney(nextValue);
  }

  handleInputOnChange = (e) => {
    this.valueOfAccount = +e.target.value;
  }

  validateString = (string) => {
    if (!Number.isInteger(string)) {
      console.error(`type of input is'nt a Number`);
      return false;
    } else if (string <= 0) {
      console.error(`value of input is negative or zero`);
      return false;
    } else {
      return true;
    }
  }
 
  render() {
    const { money } = this.props.main
    return(
      <div>
        <h1>Money on your account: {money}</h1>
        <button onClick={this.handleDepositMoney}>DEPOSIT</button>
        <button onClick={this.handleWithdrawMoney}>WITHDRAW</button>
        <input onChange={this.handleInputOnChange} type="text"/>
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        main: state.main,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Actions: bindActionCreators(Actions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
