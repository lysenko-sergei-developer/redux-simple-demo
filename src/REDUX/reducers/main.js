import {
    DEPOSIT_MONEY,
    WITHDRAW_MONEY
} from '../actions/constants'

const initialState = {
    money: 0,
};

export default function main(state = initialState, action) {
  switch(action.type) {
    case DEPOSIT_MONEY:
      return { ...state, money: action.payload };
    
    case WITHDRAW_MONEY:
      return { ...state, money: action.payload};
    
    default:
      return state;
  }
}