import {
    DEPOSIT_MONEY,
    WITHDRAW_MONEY
} from './constants'

export function depositMoney(value) {
    return {
        type: DEPOSIT_MONEY,
        payload: value
    }
}

export function withdrawMoney(value) {
    return {
        type: WITHDRAW_MONEY,
        payload: value
    }
}

